// TODO: дополнить определение класса размерами и позицией
class Square(x: Int, y: Int, name: String, var a: Int) : Movable, Figure(x, y, name), Transforming {
    // TODO: унаследовать от Figure, реализовать area()
    // TODO: реализовать интерфейс Transforming

    override fun area(): Float {
        return (a * a).toFloat()
    }

    override fun resize(zoom: Int) {
        a *= zoom
    }

    override fun rotate(direction: RotateDirection, centerX: Int, centerY: Int) {
        val y1 = y
        if (direction == RotateDirection.Clockwise) {
            y = centerY - (x - centerX)
            x = centerX + (y1 - centerY)
        } else if (direction == RotateDirection.CounterClockwise) {
            y = centerY + (x - centerX)
            x = centerX - (y1 - centerY)
        }
    }

    override fun move(dx: Int, dy: Int) {
        x += dx
        y += dy
    }

    override fun toString(): String {
        return "$name. Center: ($x,$y), width or height: $a"
    }
}