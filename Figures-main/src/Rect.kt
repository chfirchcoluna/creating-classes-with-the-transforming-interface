// сочетание определения класса и конструктора одновременно объявляет переменные и задаёт их значения
class Rect(x: Int, y: Int, name: String, var width: Int, var height: Int) : Movable, Figure(x, y, name), Transforming {
    // TODO: реализовать интерфейс Transforming
    var color: Int = -1 // при объявлении каждое поле нужно инициализировать

    // lateinit var name: String // значение на момент определения неизвестно (только для объектных типов)
    // дополнительный конструктор вызывает основной
    constructor(rect: Rect) : this(rect.x, rect.y, rect.name, rect.width, rect.height)

    // для каждого класса area() определяется по-своему
    override fun area(): Float {
        return (width*height).toFloat() // требуется явное приведение к вещественному числу
    }

    override fun resize(zoom: Int) {
        width *= zoom
        height *= zoom
    }

    override fun rotate(direction: RotateDirection, centerX: Int, centerY: Int) {
        val width1 = width
        width = height
        height = width1
        val y1 = y
        if (direction == RotateDirection.Clockwise) {
            y = centerY - (x - centerX)
            x = centerX + (y1 - centerY)
        } else if (direction == RotateDirection.CounterClockwise) {
            y = centerY + (x - centerX)
            x = centerX - (y1 - centerY)
        }
    }

    override fun move(dx: Int, dy: Int) {
        x += dx
        y += dy
    }

    override fun toString(): String {
        return "$name. Center: ($x,$y), width: $width, height: $height"
    }
}