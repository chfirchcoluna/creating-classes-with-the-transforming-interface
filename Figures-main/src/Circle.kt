import kotlin.math.PI

// TODO: дополнить определение класса размерами и позицией
class Circle(x: Int, y: Int, name: String, var r: Int) : Figure(x, y, name), Transforming, Movable {
    // TODO: реализовать интерфейс Transforming
    override fun area(): Float {
        return  (PI * r * r).toFloat();
    }

    override fun resize(zoom: Int) {
        r *= zoom
    }

    override fun rotate(direction: RotateDirection, centerX: Int, centerY: Int) {
        val y1 = y
        if (direction == RotateDirection.Clockwise) {
            y = centerY - (x - centerX)
            x = centerX + (y1 - centerY)
        } else if (direction == RotateDirection.CounterClockwise) {
            y = centerY + (x - centerX)
            x = centerX - (y1 - centerY)
        }
    }

    override fun move(dx: Int, dy: Int) {
        x += dx
        y += dy
    }

    override fun toString(): String {
        return "$name. Center: ($x,$y), radius: $r"
    }
}