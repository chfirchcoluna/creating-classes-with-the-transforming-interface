Demonstration
```
MovableRect0. Center: (0,0), width: 1, height: 1
Move MovableRect0 TO (x: 5, y: 5)
MovableRect0. Center: (5,5), width: 1, height: 1
Rect0. Center: (4,3), width: 4, height: 2
Circle0. Center: (3,3), radius: 3
Square0. Center: (-3,-5), width or height: 5
Rect0 area is
8.0
Circle0 area is
28.274334
Square0 area is
25.0

Rect1. Center: (5,5), width: 4, height: 2
Add Move Rect1 (x: 5, y: 5)
Rect1. Center: (10,10), width: 4, height: 2
Clockwise Rect1 rotation from center (x: 0, y: 0)
Rect1. Center: (10,-10), width: 2, height: 4
CounterClockwise Rect1 rotation from center (x: 3, y: -3)
Rect1. Center: (10,4), width: 4, height: 2
Increase size 3x
Rect1. Center: (10,4), width: 12, height: 6

Circle1. Center: (4,3), radius: 4
Add Move Circle1 (x: 5, y: 5)
Circle1. Center: (9,8), radius: 4
Clockwise Circle1 rotation from center (x: 3, y: -3)
Circle1. Center: (14,-9), radius: 4
CounterClockwise Circle1 rotation from center (x: 3, y: -3)
Circle1. Center: (9,8), radius: 4
Increase size 3x
Circle1. Center: (9,8), radius: 12

Square1. Center: (4,3), width or height: 4
Add Move Square1 (x: 5, y: 5)
Square1. Center: (9,8), width or height: 4
Clockwise Square1 rotation from center (x: 3, y: -3)
Square1. Center: (14,-9), width or height: 4
CounterClockwise Square1 rotation from center (x: 3, y: -3)
Square1. Center: (9,8), width or height: 4
Increase size 3x
Square1. Center: (9,8), width or height: 12
```
